#include "key.h"
#include "delay.h"
#include "led.h"
#include "main.h"
//////////////////////////////////////////////////////////////////////////////////
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//按键输入驱动代码
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2014/5/3
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved
//////////////////////////////////////////////////////////////////////////////////

//void EXTIX1_Init(void)
//{
//    NVIC_InitTypeDef   NVIC_InitStructure;
//    EXTI_InitTypeDef   EXTI_InitStructure;

//    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);//使能SYSCFG时钟


//    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOE, EXTI_PinSource0);//PE0 连接到中断线0
//    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOE, EXTI_PinSource1);//PE1 连接到中断线1

//    /* 配置EXTI_Line0,1 */
//    EXTI_InitStructure.EXTI_Line = EXTI_Line0 | EXTI_Line1;
//    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;//中断事件
//    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling; //下降沿触发
//    EXTI_InitStructure.EXTI_LineCmd = ENABLE;//中断线使能
//    EXTI_Init(&EXTI_InitStructure);//配置

//    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;//外部中断0
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x07;//抢占优先级0
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;//子优先级2
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;//使能外部中断通道
//    NVIC_Init(&NVIC_InitStructure);//配置
//	
//	NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;//外部中断0
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x08;//抢占优先级0
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;//子优先级2
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;//使能外部中断通道
//    NVIC_Init(&NVIC_InitStructure);//配置
//}

////外部中断0服务程序
//void EXTI0_IRQHandler(void)
//{
//	delay_ms(10);	//消抖
//	if(PE0 == 0)
//	{
//		LED0 = !LED0;
//		ENA_UP_DOWN1 = MOTOR_ENABLE;
//	}	
//    EXTI_ClearITPendingBit(EXTI_Line0); //清除LINE0上的中断标志位
//}
////外部中断1服务程序
//void EXTI1_IRQHandler(void)
//{
//	delay_ms(10);	//消抖
//	if(PE1 == 0)
//	{
//		LED1 = !LED1;
//		ENA_UP_DOWN1 = MOTOR_DISABLE;
//	}
//    EXTI_ClearITPendingBit(EXTI_Line1);//清除LINE2上的中断标志位
//}

//按键初始化函数
void KEY_Init(void)
{

    GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);//使能GPIOA,GPIOE时钟

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4; //KEY0 KEY1 KEY2对应引脚
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
    GPIO_Init(GPIOE, &GPIO_InitStructure);//初始化GPIOE2,3,4
	
	//EXTIX1_Init();
}




















