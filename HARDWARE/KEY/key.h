#ifndef __KEY_H
#define __KEY_H
#include "sys.h"
//////////////////////////////////////////////////////////////////////////////////
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//按键输入驱动代码
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2014/5/3
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved
//////////////////////////////////////////////////////////////////////////////////


/*下面方式是通过位带操作方式读取IO*/

#define PE0 		PEin(0)   	//PE4
#define PE1 		PEin(1)		//PE3
#define PE2 		PEin(2)		//P32
#define PE3 		PEin(3)		//PA0



void KEY_Init(void);	//IO初始化

#endif
