#ifndef __INPUT_H
#define __INPUT_H
#include "sys.h"
//////////////////////////////////////////////////////////////////////////////////
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//按键输入驱动代码
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2014/5/3
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved
//////////////////////////////////////////////////////////////////////////////////


/*下面方式是通过位带操作方式读取IO*/

#define PG0 		PGin(0)   	
#define PG1 		PGin(1)		
#define PG2 		PGin(2)		
#define PG3 		PGin(3)		
#define PG4 		PGin(4)   	
#define PG5 		PGin(5)		
#define PG6 		PGin(6)		
#define PG7 		PGin(7)	
#define PG8 		PGin(8)		
#define PG9 		PGin(9)	

void INPUT_IO_INIT(void);	//IO初始化

#endif
