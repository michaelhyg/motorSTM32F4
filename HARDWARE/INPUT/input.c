#include "input.h"
#include "delay.h"
#include "led.h"
#include "main.h"

//////////////////////////////////////////////////////////////////////////////////
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//按键输入驱动代码
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2014/5/3
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved
//////////////////////////////////////////////////////////////////////////////////

//void EXTIX_Init(void)
//{
//    NVIC_InitTypeDef   NVIC_InitStructure;
//    EXTI_InitTypeDef   EXTI_InitStructure;

//    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);//使能SYSCFG时钟

//	//PE0 连接到中断线0
//    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource0);
//    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource1);
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource2);
//    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource3);
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource4);
//    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource5);
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource6);
//    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource7);
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource8);
//    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOG, EXTI_PinSource9);

//	
//    /* 配置EXTI_Line0,1 */
//    EXTI_InitStructure.EXTI_Line = EXTI_Line0|EXTI_Line1|EXTI_Line2|EXTI_Line3|EXTI_Line4|EXTI_Line5|EXTI_Line6|EXTI_Line7|EXTI_Line8|EXTI_Line9;
//    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;//中断事件
//    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling; //下降沿触发
//    EXTI_InitStructure.EXTI_LineCmd = ENABLE;//中断线使能
//    EXTI_Init(&EXTI_InitStructure);//配置

//    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;//外部中断0
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;//抢占优先级0
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;//子优先级2
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;//使能外部中断通道
//    NVIC_Init(&NVIC_InitStructure);//配置
//	
//	
//	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;//外部中断0
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;//抢占优先级0
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;//子优先级2
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;//使能外部中断通道
//    NVIC_Init(&NVIC_InitStructure);//配置

//}

////外部中断0服务程序
//void EXTI0_IRQHandler(void)
//{
//	delay_ms(10);	//消抖
//	if(PG0 == 0)
//	{
//		LED0 = !LED0;
//		ENA_UP_DOWN1 = MOTOR_ENABLE;
//	}	
//    EXTI_ClearITPendingBit(EXTI_Line0); //清除LINE0上的中断标志位
//}
////外部中断1服务程序
//void EXTI1_IRQHandler(void)
//{

//}

//void EXTI2_IRQHandler(void)
//{
//	
//}

//void EXTI3_IRQHandler(void)
//{

//}

//void EXTI4_IRQHandler(void)
//{

//}

//void EXTI9_5_IRQHandler(void)
//{

//}


//输入IO口初始化
void INPUT_IO_INIT(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);//使能GPIOF时钟

    //GPIOF9,F10初始化设置
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|\
	GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10;
	
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//上拉
    GPIO_Init(GPIOG, &GPIO_InitStructure);//初始化
	
	//EXTIX_Init();
}



















