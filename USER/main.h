#ifndef __MAIN_H
#define __MAIN_H

#include "sys.h"

#define UP  1
#define DOWN 0

#define BACK  1
#define FRONT 0


//TEST
#define DIR PDout(3)
#define PUL PIout(5)
#define ENA PDout(7)
//


#define MOTOR_DISABLE		1
#define MOTOR_ENABLE		0

#define DIR_FRONT_BACK1 PDout(3)
#define ENA_FRONT_BACK1	PDout(7)


#define DIR_FRONT_BACK2	PDout(11)
#define ENA_FRONT_BACK2	PFout(11)



#define DIR_UP_DOWN1 	PFout(1)
#define ENA_UP_DOWN1	PFout(2)

                                                      
#define DIR_UP_DOWN2 	PCout(8)
#define ENA_UP_DOWN2	PHout(7)



#define ORIGIN1	PGin(0)	//原点位置感应器，0表示触发
#define FRONT1	PGin(1)	//前位置感应器
#define BELOW1	PGin(2)	//底部位置感应，0表示触发
#define UPPER1	PGin(3) //上部位置感应器
#define DISTANCE1	PGin(8) //距离传感器

#define ORIGIN2	PGin(4)	//原点位置感应器，0表示触发
#define FRONT2	PGin(5)	//前位置感应器
#define BELOW2	PGin(6)	//底部位置感应，0表示触发
#define UPPER2	PGin(7) //上部位置感应器

#define DISTANCE2	PGin(9) //距离传感器


#define TRIG1 PBin(14)	//扫码触发信号1
#define TRIG2 PBin(15)	//扫码触发信号2


#endif