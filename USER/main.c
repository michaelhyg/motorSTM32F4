#include "main.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "pwm.h"
#include "key.h"
#include "input.h"
#include <string.h>

u16 Distance;	//运动的距离
char  Data_buff[200]; 


//电机IO口初始化
void Motor_IO_INIT(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOF | RCC_AHB1Periph_GPIOH, ENABLE);//使能GPIOF时钟

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_7 | GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
    GPIO_Init(GPIOD, &GPIO_InitStructure);//初始化
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
    GPIO_Init(GPIOF, &GPIO_InitStructure);//初始化
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
    GPIO_Init(GPIOC, &GPIO_InitStructure);//初始化
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
    GPIO_Init(GPIOH, &GPIO_InitStructure);//初始化
}

void MOVE(u8 dir)
{
	DIR_FRONT_BACK1 = dir;
	DIR_FRONT_BACK2 = dir;
	ENA_FRONT_BACK1 = MOTOR_ENABLE;	
	ENA_FRONT_BACK2 = MOTOR_ENABLE;
	TIM8_PWM_Init(500,70);
	delay_ms(100);
	TIM8_PWM_Init(500,50);
	delay_ms(100);
	TIM8_PWM_Init(500,30);
	delay_ms(100);
	TIM8_PWM_Init(500,20);
	delay_ms(100);
	TIM8_PWM_Init(500,13);
}

void MOVE1(u8 dir)
{
	DIR_FRONT_BACK1 = dir;
	ENA_FRONT_BACK1 = MOTOR_ENABLE;
	TIM8_PWM_Init(500,70);
	delay_ms(100);
	TIM8_PWM_Init(500,50);
	delay_ms(100);
	TIM8_PWM_Init(500,30);
	delay_ms(100);
	TIM8_PWM_Init(500,20);
	delay_ms(100);
	TIM8_PWM_Init(500,13);
}

void MOVE2(u8 dir)
{
	DIR_FRONT_BACK2 = dir;
	ENA_FRONT_BACK2 = MOTOR_ENABLE;
	TIM8_PWM_Init(500,70);
	delay_ms(100);
	TIM8_PWM_Init(500,50);
	delay_ms(100);
	TIM8_PWM_Init(500,30);
	delay_ms(100);
	TIM8_PWM_Init(500,20);
	delay_ms(100);
	TIM8_PWM_Init(500,13);
}
int main(void)
{
	char trig_flag = 0;
	u16 len;
	char on_off_flag = 1;
	char ready_flag = 1;
	u16 t;
	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置系统中断优先级分组2
    delay_init(168);  //初始化延时函数
    uart_init(9600);//初始化串口波特率为115200
	
	
	LED_Init();
	Motor_IO_INIT();
	KEY_Init();
	INPUT_IO_INIT();
	
	
	LED0 = 1;
	LED1 = 1;

	
	ENA_UP_DOWN1 = MOTOR_DISABLE;
	ENA_UP_DOWN2 = MOTOR_DISABLE;
	ENA_FRONT_BACK1 = MOTOR_DISABLE;
	ENA_FRONT_BACK2 = MOTOR_DISABLE;
	
	//TIM8_PWM_Init(0xFFFF,5);
	//TIM_SetCompare1(TIM8,200);
	//TIM8_PWM_Init_xxx(500-1,84-1,1,200);	//84M/84=1Mhz的计数频率,重装载值500，所以PWM频率为 1M/500=2Khz. 
	//test();
	
	printf("start\r\n");

    while(1)
    {
		if(USART_RX_STA&0x8000)
        {
            len=USART_RX_STA&0X3FFF;
            memcpy(Data_buff,USART_RX_BUF,len);
            USART_RX_STA=0;
            if(strstr(Data_buff,"go"))
            {
				trig_flag = 1;
				ready_flag = 1;
				LED0 = !LED0;
            }
			if(strstr(Data_buff,"back"))
            {
				trig_flag = 2;
            }
			if(strstr(Data_buff,"back"))
            {
				trig_flag = 2;
            }
			if(strstr(Data_buff,"on"))
            {
				printf("ONOK\r\n");
				on_off_flag = 1;
				trig_flag = 1;
            }
			if(strstr(Data_buff,"off"))
            {
				printf("OFFOK\r\n");
				on_off_flag = 0; 
            }
			if(strstr(Data_buff,"ready"))
            {
				printf("ready\r\n");
				ready_flag = 1;
            }
            USART_RX_STA = 0;
			
			memset(Data_buff,0,sizeof(Data_buff));
            memset(USART_RX_BUF,0,sizeof(USART_RX_BUF));
        }
		
		if(ready_flag == 0)
		{
			delay_ms(10);
			t++;
			if(t == 2000)
			{
				t = 0;
				LED0 = !LED0;
				ready_flag = 1;
			}
		}
		
		if((on_off_flag == 1)&&(ready_flag == 1))
		{			
			if(DISTANCE1 == 0)
			{
				delay_ms(50);
				if(DISTANCE1 == 0)
				{
					trig_flag = 1;
				}
			}

			
			if(trig_flag == 1)
			{
				ready_flag = 0;
				/*
				*前后装置动作，往前伸出
				*/	
				MOVE(FRONT);			
				while(1)
				{
					if(FRONT1 == 0)
					{
						ENA_FRONT_BACK1 = MOTOR_DISABLE;	//位置传感器或者距离传感器感应到则停止	
						ENA_FRONT_BACK2 = MOTOR_DISABLE;	//位置传感器或者距离传感器感应到则停止		
						break; 					
					}
					if(FRONT2 == 0)
					{
						ENA_FRONT_BACK1 = MOTOR_DISABLE;	//位置传感器或者距离传感器感应到则停止	
						ENA_FRONT_BACK2 = MOTOR_DISABLE;	//位置传感器或者距离传感器感应到则停止
						break; 
					}			
				}
				delay_ms(500);

				/*
				*向上运动
				*/	
				TIM8_PWM_Init(500,50);
				DIR_UP_DOWN1 = UP;	
				DIR_UP_DOWN2 = UP;			
				ENA_UP_DOWN1 = MOTOR_ENABLE;
				ENA_UP_DOWN2 = MOTOR_ENABLE;
				
				delay_ms(1000);			
				delay_ms(1000);				

				TIM_Cmd(TIM8, DISABLE);
		
				delay_ms(500);
				
				
				/*
				*向下运动
				*/
				TIM8_PWM_Init(500,50);
				DIR_UP_DOWN1 = DOWN;
				DIR_UP_DOWN2 = DOWN;
				ENA_UP_DOWN1 = MOTOR_ENABLE;
				ENA_UP_DOWN2 = MOTOR_ENABLE;
				while(trig_flag)
				{
					if(BELOW1 == 0)
					{
						ENA_UP_DOWN1 = MOTOR_DISABLE;;	//已经运到到底部
						ENA_UP_DOWN2 = MOTOR_DISABLE;;	//已经运到到底部
						break;
					}
					if(BELOW2 == 0)
					{
						ENA_UP_DOWN1 = MOTOR_DISABLE;;	//已经运到到底部
						ENA_UP_DOWN2 = MOTOR_DISABLE;;	//已经运到到底部
						break;
					}
				}
				delay_ms(500);
				
				/*
				*向后运动
				*/				
				MOVE(BACK);
				while(trig_flag)
				{
					if(ORIGIN1 == 0)
					{
						ENA_FRONT_BACK1 = MOTOR_DISABLE;
						ENA_FRONT_BACK2 = MOTOR_DISABLE;
						break;
					}
					if(ORIGIN2 == 0)
					{
						ENA_FRONT_BACK1 = MOTOR_DISABLE;
						ENA_FRONT_BACK2 = MOTOR_DISABLE;
						break;
					}
				}
				trig_flag = 0;	//运动完成清除标志位
				printf("OK\r\n");
				t = 0;
			}
			else if(trig_flag == 2)
			{
				/*
				*向下运动
				*/
				TIM8_PWM_Init(500,80);
				DIR_UP_DOWN1 = DOWN;
				DIR_UP_DOWN2 = DOWN;
				ENA_UP_DOWN1 = MOTOR_ENABLE;
				ENA_UP_DOWN2 = MOTOR_ENABLE;
				while(1)
				{
					if(BELOW1 == 0)
					{
						ENA_UP_DOWN1 = MOTOR_DISABLE;;	//已经运到到底部
						ENA_UP_DOWN2 = MOTOR_DISABLE;;	//已经运到到底部
						break;
					}
					if(BELOW2 == 0)
					{
						ENA_UP_DOWN1 = MOTOR_DISABLE;;	//已经运到到底部
						ENA_UP_DOWN2 = MOTOR_DISABLE;;	//已经运到到底部
						break;
					}
				}
				/*
				*向后运动
				*/				
				MOVE(BACK);
				while(trig_flag)
				{
					if(ORIGIN1 == 0)
					{
						ENA_FRONT_BACK1 = MOTOR_DISABLE;
						ENA_FRONT_BACK2 = MOTOR_DISABLE;
						break;
					}
					if(ORIGIN2 == 0)
					{
						ENA_FRONT_BACK1 = MOTOR_DISABLE;
						ENA_FRONT_BACK2 = MOTOR_DISABLE;
						break;
					}
				}
				trig_flag = 0;	//运动完成清除标志位
				printf("back end\r\n");
			}
			
		}
		
		
		
		if(PE0 == 0)
		{
			delay_ms(50);
			while(PE0 == 0);			
			LED1 = !LED1;
			
			MOVE1(BACK);
	
			while(1)
			{
				if(ORIGIN2 == 0)
				{
					ENA_FRONT_BACK2 = MOTOR_DISABLE;	//位置传感器或者距离传感器感应到则停止	
					break;
				}
			}
		}
		if(PE1 == 0)
		{
			delay_ms(50);
			while(PE1 == 0);
			LED0 = !LED0;
			MOVE1(FRONT);
			delay_ms(2000);
			ENA_FRONT_BACK2 = MOTOR_DISABLE;
		}
		if(PE2 == 0)
		{
			trig_flag = 1;
		}
		if(PE3 == 0)
		{
			trig_flag = 2;
		}		
    }
}
